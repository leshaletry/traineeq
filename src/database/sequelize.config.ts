const { Sequelize } = require("sequelize");
import "dotenv/config";

var sequelize = new Sequelize(
  `postgres://postgres:${process.env.DB_PASSWORD}@localhost:${process.env.DB_PORT}/quiz`
);

export default sequelize;
