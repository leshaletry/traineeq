import {
  Context,
  NextMiddleware,
  OptionsSource,
  SlackAction,
  SlackActionMiddlewareArgs,
  SlackCommandMiddlewareArgs,
  SlackEventMiddlewareArgs,
  SlackOptionsMiddlewareArgs,
  SlackViewMiddlewareArgs
} from "@slack/bolt";
import {
  Block,
  Button,
  Datepicker,
  ImageElement,
  MultiSelect,
  Overflow,
  Select
} from "@slack/types";

export type OptionsMiddlewareArgs<
  T extends OptionsSource = OptionsSource
> = SlackOptionsMiddlewareArgs<T> & {
  next: NextMiddleware;
  context: Context;
};

export type CommandMiddlewareArgs = SlackCommandMiddlewareArgs & {
  next: NextMiddleware;
  context: Context;
};

export type MessageMiddlewareArgs = SlackEventMiddlewareArgs<"message"> & {
  next: NextMiddleware;
  context: Context;
};

export type EventMiddlewareArgs<
  T extends string = string
> = SlackEventMiddlewareArgs<T> & {
  next: NextMiddleware;
  context: Context;
};

export type ActionMiddlewareArgs<
  T extends SlackAction
> = SlackActionMiddlewareArgs<T> & {
  next: NextMiddleware;
  context: Context;
};

export type ViewMiddlewareArgs = SlackViewMiddlewareArgs & {
  next: NextMiddleware;
  context: Context;
};

export type SlackMiddlewareArgs =
  | ActionMiddlewareArgs<SlackAction>
  | ViewMiddlewareArgs
  | CommandMiddlewareArgs
  | EventMiddlewareArgs
  | MessageMiddlewareArgs;

export type InteractiveHandler<T extends SlackMiddlewareArgs> = (
  args: T
) => Promise<void>;
