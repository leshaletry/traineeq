import { app } from "./app";
import pool from "./database/db.config";
import sequelize from "./database/sequelize.config";

((async () => {
  await app.start(8080);
  console.log("⚡️ Bolt app is running!");
  await sequelize.sync({});
  console.log("⚡️ DB Connected!");
}))();
