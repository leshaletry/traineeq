export function errorHandle(actionHandler: Function) {
  return async function(args: any) {
    try {
      await actionHandler(args);
    } catch (error) {
      console.log(error.data.response_metadata);
    }
  };
}
